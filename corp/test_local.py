from corp.settings import * # noqa: F403

DEPARTMENTS_SOURCE = 'tests/test_static_data/departments.json'
OFFICES_SOURCE = 'tests/test_static_data/offices.json'

DEPARTMENTS = readJson(DEPARTMENTS_SOURCE)
OFFICES = readJson(OFFICES_SOURCE)
