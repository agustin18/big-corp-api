from django.conf.urls import (
    include,
    url,
)
from rest_framework.routers import DefaultRouter
from rest_framework_swagger.views import get_swagger_view

from views import (
    EmployeesViewSet,
    OfficesViewSet,
    DepartmentsViewSet,
)


schema_view = get_swagger_view(title='Agustin Saiz Code Challenge')

corp_router = DefaultRouter()

corp_router.register(r'employees', EmployeesViewSet, basename='employ')
corp_router.register(r'offices', OfficesViewSet, basename='offic')
corp_router.register(r'departments', DepartmentsViewSet, basename='dptos')

urlpatterns = [
    url(r'', include((corp_router.urls, 'app'), namespace='app')),
    url(r'^swagger/$', schema_view),
]
