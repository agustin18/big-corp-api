import requests
from django.conf import settings

from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response

from expansions import get_expansions
from pagination import (
    BigCorpPagination,
    get_limit,
    get_offset,
)
from utils import EmployeesThrottle


class EmployeesViewSet(GenericViewSet):
    queryset = []
    pagination_class = BigCorpPagination
    throttle_classes = [EmployeesThrottle]

    def list(self, request, *args, **kwargs):
        """
        check limit and offset if are specified, to fail quickly if are wrong or
        set them if are not. Then we go to the source of the endpoint to get the data
        and finally we process the expansions if they are come into the request
        """
        limit = get_limit(self.pagination_class, self.request.query_params.get('limit'))
        offset = get_offset(self.request.query_params.get('offset'))
        url = '{}?limit={}&offset={}'.format(settings.EMPLOYEE_SOURCE, limit, offset)
        employees = requests.get(url).json()
        expansions = request.query_params.getlist('expand')
        if expansions:
            employees = get_expansions(employees, expansions, 'employee')
        return Response(employees)

    def retrieve(self, request, pk=None):
        employee = requests.get('{}?id={}'.format(settings.EMPLOYEE_SOURCE, pk)).json()
        expansions = request.query_params.getlist('expand')
        if expansions:
            employee = get_expansions(employee, expansions, 'employee')
        return Response(employee)


class DepartmentsViewSet(GenericViewSet):
    queryset = []
    pagination_class = BigCorpPagination

    def list(self, request, *args, **kwargs):
        limit = get_limit(self.pagination_class, self.request.query_params.get('limit'))
        offset = get_offset(self.request.query_params.get('offset'))
        departments = [e for e in settings.DEPARTMENTS if e.get('id') > offset][:limit]
        expansions = request.query_params.getlist('expand')
        if expansions:
            departments = get_expansions(departments, expansions, 'department')
        return Response(departments)

    def retrieve(self, request, pk=None):
        department = [e for e in settings.DEPARTMENTS if e.get('id') == int(pk)]
        expansions = request.query_params.getlist('expand')
        if expansions:
            department = get_expansions(department, expansions, 'department')
        return Response(department)


class OfficesViewSet(GenericViewSet):
    queryset = []
    pagination_class = BigCorpPagination

    def list(self, request, *args, **kwargs):
        """
        office view doesn't allow expansions in order to not process pointless queryparams
        we don't consider them even if they are present in the request
        """
        limit = get_limit(self.pagination_class, self.request.query_params.get('limit'))
        offset = get_offset(self.request.query_params.get('offset'))
        offices = [e for e in settings.OFFICES if e.get('id') > offset][:limit]
        return Response(offices)

    def retrieve(self, request, pk=None):
        office = [e for e in settings.OFFICES if e.get('id') == int(pk)]
        return Response(office)
