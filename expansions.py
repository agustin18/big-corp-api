from django.conf import settings

import requests

ALLOWED_EXPANSIONS = {
    'employee': ['manager', 'office', 'department'],
    'manager': ['manager', 'office', 'department'],
    'department': ['superdepartment'],
    'superdepartment': ['superdepartment']
}


def get_expansions(data, expansions, origin):
    for expansion in expansions:
        # Split the expansion path
        expansion_parameters = expansion.split('.')
        # process the expansion
        data = process_expansion(data, expansion_parameters, origin)

    return data


def process_expansion(data, parameters, origin):
    # Get one parameter and remove it from the expansion parameters
    current_parameter = parameters.pop(0)

    # Process the parameter and get the expansion data
    expansion_data = get_expansion_data(data, current_parameter, origin)

    # Call the process_expansion function if more parameters are remaning
    if parameters:
        origin = current_parameter
        expansion_data = process_expansion(expansion_data, parameters, origin)

    # Inedex the data received in a dictionary it's easier than a list
    indexed_data = {elem['id']: elem for elem in expansion_data}
    # Expand the elements of the data
    for element in data:
        if isinstance(element[current_parameter], dict):
            id_to_expand = element[current_parameter].get('id')
        else:
            id_to_expand = element[current_parameter]
        element[current_parameter] = indexed_data.get(id_to_expand, None)
    return data


def get_expansion_data(data, parameter, origin):
    # Is an allowed expansion for the given origin
    if parameter not in ALLOWED_EXPANSIONS[origin]:
        raise KeyError('Expansion {} not allowed for {} entity'.format(parameter, origin))

    # Get the ids of the parameter to be expanded
    ids = [obj[parameter] for obj in data if obj.get(parameter) is not None]

    # if there is no ids, break the process and return
    if not ids:
        return []

    # check if the keys are dictonaries, case when expanded before
    if all([isinstance(key, dict) for key in ids]):
        ids = [obj.get('id') for obj in ids]

    # Remove repeted ids
    ids = set(ids)
    # 'manager' should be hit the api to get the necessary data
    if parameter == 'manager':
        expansion_data = requests.get(settings.EMPLOYEE_SOURCE, params={'id': ids}).json()
    # 'office' and 'department' should go to the static data
    elif parameter == 'office':
        expansion_data = [e for e in settings.OFFICES if e.get('id') in ids]
    elif parameter in ['department', 'superdepartment']:
        expansion_data = [e for e in settings.DEPARTMENTS if e.get('id') in ids]

    return expansion_data
