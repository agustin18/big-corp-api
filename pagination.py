from rest_framework.pagination import LimitOffsetPagination


class BigCorpPagination(LimitOffsetPagination):
    default_limit = 100
    max_limit = 1000
    limit_query_param = "limit"
    offset_query_param = "offset"


# get_limit methods is no thied to any Pagintion class to be ready to handle different if it's necessary
def get_limit(pagination_class, limit=None):
    if limit:
        limit = int(limit)
        if limit > pagination_class.max_limit:
            raise ValueError('limit should be less or equal to {}'.format(pagination_class.max_limit))
        elif limit <= 0:
            raise ValueError('Limit Should be major than 0')
    else:
        limit = pagination_class.default_limit
    return limit


def get_offset(offset=None):
    if offset:
        offset = int(offset)
        if offset < 0:
            raise ValueError('Offset Should be major than 0')
    else:
        offset = 0
    return offset
