# Big Corp API

Service to access to the company employee information.

## How to run the project

### Docker

    1. Build image (docker build . -t corp-api)
    2. Run the project (docker run -it -p 8010:8000 corp-api) -you can change 8010 for the port that you want to use
    3. Run tests (docker run corp-api pytest --cov)


### Virtualenv
    1. Create python3.7 virtualenv (python3.7 -m venv bigcorpenv)
    2. Activate venv (. bigcorpenv/bin/activate)
    3. Upgrade pip (pip install --upgrade pip)
    5. Install dependencies (pip install -r ./requirements.txt)
    6. Runserver (python ./manage.py runserver)
    7. Run tests (pytest --cov)

## Project Overview

Build a web app with a read-only JSON API for 3 resources, employees, departments, offices, you can use any framework or language for building your app.

### Data Sources

The data source for employees is the API:

`https://rfy56yfcwk.execute-api.us-west-1.amazonaws.com/bigcorp/employees`

it supports two ways of querying, with limit in offset:
   1. `https://rfy56yfcwk.execute-api.us-west-1.amazonaws.com/bigcorp/employees?limit=10&offset=20` (gets 21 th through 31st record) or querying multiple ids
   2. `https://rfy56yfcwk.execute-api.us-west-1.amazonaws.com/bigcorp/employees?id=3&id=4&id=5` (returns records with id=3, id=4, id=5)

The data source for offices and departments are the files offices.json, departments.json. These
are the entire list of offices and departments, and you can just read all the data for each and keep it in memory in your app.

### API Design

Your app should support GET requests to two endpoints (list & detail) for each resource following a
standard REST convention
detail e.g. `/employees/1`
list e.g. `/employees`

* list should support query limit and offset to support pagination.
* limit is the max number of records returned, and offset is the index at which to start.
for instance /employees?limit=10&offset=17 returns the 18th through 27th employee By
default, the limit is 100 and the max limit is 1000
* Both methods should support a query parameter called to expand that lets you expand
data along paths of to-one relationships
* There are four relationships that can be expanded:
    1. manager in employees (expands to employees )
    2. office in employees (expands to offices )
    3. department in employees (expands to departments )
    4. superdepartment in departments (expands to departments )


### Notes and Acceptance Criteria
* There is no need to build any kind of POST or other write api. Also, there's no need to use a
database or caching system of any kind for this problem.
* Avoid overuse of employees api endpoint. For instance, requests like /employees?
limit=1000&offset=10000&expand=manager.manager.manager should not make thousands or
even hundreds of calls to the endpoint.
* Well designed APIs
* Well structured and reusable abstractions

## Testing

In order to not use our actual sources, in the testing, I replicated the .json files for testing purposes in another directory and override our settings variable to use others in the test runs.

For the case of the employee API endpoint, I replicated a large response and use pytest fixtures as a source of the employee data, to not use the actual endpoint, but at the same time test the code and its functionality

Also, I configured Coverage to see how much testing coverage I have, and is 100% percent at this moment. In order to not reduce this number, I added a CI process to the project to double-check the results and the coverage, every time a commit reach the remote repository

The tests done it here cover the acceptance criteria, we are not using any kind of cache system, but we only do one call to the employee API endpoint per expansion asked so even in the extreme cases like `/employees?
limit=1000&offset=10000&expand=manager.manager.manager` it will be hit the endpoint 4 times.

### API Documentation

In addition, I added the first version of API documentation, with Swagger. You can access in the path `/swagger`
