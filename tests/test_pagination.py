from pagination import (
    BigCorpPagination,
    get_limit,
    get_offset,
)
import pytest


class TestPagination:

    def test_get_limit_default(self):
        response = get_limit(BigCorpPagination)
        assert response == BigCorpPagination.default_limit

    def test_get_limit_custom(self):
        response = get_limit(BigCorpPagination, '50')
        assert response == 50

    def test_get_limit_error_max(self):
        with pytest.raises(ValueError) as error:
            get_limit(BigCorpPagination, BigCorpPagination.max_limit + 1)
        assert "limit should be less or equal to {}".format(BigCorpPagination.max_limit) in str(error.value)

    def test_get_limit_error_(self):
        with pytest.raises(ValueError) as error:
            get_limit(BigCorpPagination, '-10')
        assert "Limit Should be major than 0" in str(error.value)

    def test_get_offset(self):
        response = get_offset('100')
        assert response == 100

    def test_get_offset_default(self):
        response = get_offset()
        assert response == 0

    def test_get_offset_error(self):
        with pytest.raises(ValueError) as error:
            get_offset('-20')
        assert "Offset Should be major than 0" in str(error.value)
