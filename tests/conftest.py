import pytest

"""
In This file you will find the pytest fixtures for test purposes.
In the pytest.ini file we override a couple of settings to not use the same files to test and
the actual traffic

"""

pytest_plugins = ['tests.fixture_employee_api_response']


@pytest.fixture
def api_client():
    from rest_framework.test import APIClient
    return APIClient()


@pytest.fixture
def intial_data_for_multiple_expansions():
    return (
        [{
            "first": "Patricia",
            "last": "Diaz",
            "id": 1,
            "manager": None,
            "department": 5,
            "office": 2
        }, {
            "first": "Daniel",
            "last": "Smith",
            "id": 2,
            "manager": 1,
            "department": 5,
            "office": 2
        }, {
            "first": "Daniel",
            "last": "Pope",
            "id": 3,
            "manager": 2,
            "department": 8,
            "office": 2
        }]
    )


@pytest.fixture
def intial_data_for_extreme_case():
    return (
        [{
            "id": 11,
            "name": "Test Office",
            "superdepartment": 9
        }]
    )


@pytest.fixture
def expanded_data_for_expansions():
    return (
        [{
            "first": "Patricia",
            "last": "Diaz",
            "id": 1,
            "manager": None,
            "department": 5,
            "office": 2
        }, {
            "first": "Daniel",
            "last": "Smith",
            "id": 2,
            "manager": 1,
            "department": 5,
            "office": 2
        }]
    )


@pytest.fixture
def expected_office_1_response():
    return (
        [{
            "id": 1,
            "city": "San Francisco Test",
            "country": "United States",
            "address": "450 Market St"
        }]
    )


@pytest.fixture
def expected_department_1_response():
    return (
        [{
            "id": 7,
            "name": "Application Security",
            "superdepartment": 2
        }]
    )


@pytest.fixture
def expected_department_1_response_with_expansion():
    return (
        [{
            "id": 7,
            "name": "Application Security",
            "superdepartment": {
                "id": 2,
                "name": "Engineering",
                "superdepartment": None,
            }
        }]
    )


@pytest.fixture
def expected_response_with_multiple_expansions():
    return(
        [
            {
                'first': 'Patricia',
                'last': 'Diaz',
                'id': 1,
                'manager': None,
                'department': 5,
                'office': 2
            },
            {
                'first': 'Daniel',
                'last': 'Smith',
                'id': 2,
                'manager': {
                    'first': 'Patricia',
                    'last': 'Diaz',
                    'id': 1,
                    'manager': None,
                    'department': 5,
                    'office': 2
                },
                'department': 5,
                'office': 2
            },
            {
                'first': 'Daniel',
                'last': 'Pope',
                'id': 3,
                'manager': {
                    'first': 'Daniel',
                    'last': 'Smith',
                    'id': 2,
                    'manager': {
                        'first': 'Patricia',
                        'last': 'Diaz',
                        'id': 1,
                        'manager': None,
                        'department': 5,
                        'office': 2
                    },
                    'department': 5,
                    'office': 2
                },
                'department': 8,
                'office': 2
            }
        ]
    )


@pytest.fixture
def expected_response_with_extreme_expansions():
    return(
        [{
            "id": 1,
            "name": "Sales",
            "superdepartment": None
        }, {
            "id": 2,
            "name": "Engineering",
            "superdepartment": None
        }, {
            "id": 3,
            "name": "Product",
            "superdepartment": None
        }, {
            "id": 4,
            "name": "Design",
            "superdepartment": {
                "id": 3,
                "name": "Product",
                "superdepartment": None
            }
        }, {
            "id": 5,
            "name": "Inbound Sales",
            "superdepartment": {
                "id": 1,
                "name": "Sales",
                "superdepartment": None
            }
        }, {
            "id": 6,
            "name": "Outbound Sales",
            "superdepartment": {
                "id": 1,
                "name": "Sales",
                "superdepartment": None
            }
        }, {
            "id": 7,
            "name": "Application Security",
            "superdepartment": {
                "id": 2,
                "name": "Engineering",
                "superdepartment": None
            }
        }, {
            "id": 8,
            "name": "Front-End",
            "superdepartment": {
                "id": 2,
                "name": "Engineering",
                "superdepartment": None
            }
        }, {
            "id": 9,
            "name": "Sales Development",
            "superdepartment": {
                "id": 6,
                "name": "Outbound Sales",
                "superdepartment": {
                    "id": 1,
                    "name": "Sales",
                    "superdepartment": None
                }
            }
        }, {
            "id": 10,
            "name": "Product Management",
            "superdepartment": {
                "id": 3,
                "name": "Product",
                "superdepartment": None
            }
        }, {
            "id": 11,
            "name": "Test Department",
            "superdepartment": {
                "id": 9,
                "name": "Sales Development",
                "superdepartment": {
                    "id": 6,
                    "name": "Outbound Sales",
                    "superdepartment": {
                        "id": 1,
                        "name": "Sales",
                        "superdepartment": None
                    }
                }
            }
        }]
    )


@pytest.fixture()
def expected_expansion_response_employee_without_manager():
    return(
        [
            {
                'first': 'Patricia',
                'last': 'Diaz',
                'id': 1,
                'manager': None,
                'department': 5,
                'office': 2
            },
        ]
    )


@pytest.fixture()
def stubbed_response_from_employee_api(fixture_employee_api_response, limit=100, offset=0, pk=None):
    """
    This fixture takes as parameter a pretty large stubed response from employee endpoint, in order
    to not use the actual endpoint.
    """
    def _get_response(limit=100, offset=0, pk=None):
        if pk:
            return [e for e in fixture_employee_api_response if e.get('id') == pk]
        return [e for e in fixture_employee_api_response if e.get('id') > offset][:limit]

    return _get_response
