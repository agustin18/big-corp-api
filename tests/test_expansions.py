from django.conf import settings

import pytest

from expansions import (
    get_expansions,
    get_expansion_data,
    process_expansion,
)


class TestExpansions:

    def test_get_expansion_data_flow(
            self,
            requests_mock,
            intial_data_for_multiple_expansions,
            expanded_data_for_expansions,
            expected_response_with_multiple_expansions,
    ):
        """
        This test is more than a simple unit test, because is testing the core part of the expansions
        and those are the get_expansions, process_expansions and the call to get_expansion_data
        in this one we are also testing that even if there are a lot of employees to get, we can
        do it with a few calls .
        """

        # We use requests mock to not use the actual API for testing
        mock_request = requests_mock.get(settings.EMPLOYEE_SOURCE, json=expanded_data_for_expansions)
        response = get_expansions(intial_data_for_multiple_expansions, ['manager.manager'], 'employee')
        # Cheking that we only call only 2 times to the API one per expansion
        assert mock_request.call_count == 2
        assert response == expected_response_with_multiple_expansions

    def test_get_expansion_data_wrong_expansion(self, intial_data_for_multiple_expansions):
        with pytest.raises(KeyError) as error:
            get_expansion_data(intial_data_for_multiple_expansions, 'manager', 'department')
        assert "Expansion manager not allowed for department entity" in str(error.value)

    def test_get_expansion_without_information(
        self,
        expected_expansion_response_employee_without_manager,
        stubbed_response_from_employee_api,
    ):
        # this case is when the employee for example doesn't have manager. It will return an empty Value
        intial_data = stubbed_response_from_employee_api(pk=1)
        response = process_expansion(intial_data, ['manager', 'manager'], 'employee')
        assert response == expected_expansion_response_employee_without_manager
