from django.conf import settings

import pytest


class TestViewOffices:
    def test_view_offices(self, api_client):
        response = api_client.get('/offices/')
        assert response.status_code == 200
        assert len(response.data) == 5
        assert response.data == settings.OFFICES

    def test_view_offices_with_pagination(self, api_client):
        response = api_client.get('/offices/?limit=2&offset=1')
        assert response.status_code == 200
        assert len(response.data) == 2
        assert response.data == settings.OFFICES[1:3]

    def test_view_offices_with_expansions(self, api_client):
        # It should return the same data, and ignore the expand queryparam
        response = api_client.get('/offices/?expand=department')
        assert response.status_code == 200
        assert len(response.data) == 5
        assert response.data == settings.OFFICES

    def test_view_office(self, api_client, expected_office_1_response):
        response = api_client.get('/offices/1/')
        assert response.status_code == 200
        assert len(response.data) == 1
        assert response.data == expected_office_1_response

    def test_view_office_with_pagination(self, api_client, expected_office_1_response):
        # The request with an id, should ignore pagination querypaarams
        response = api_client.get('/offices/1/?limit=2&offset=1')
        assert response.status_code == 200
        assert len(response.data) == 1
        assert response.data == expected_office_1_response

    def test_view_office_with_expansions(self, api_client, expected_office_1_response):
        # It should return the same data, and ignore the expand queryparam
        response = api_client.get('/offices/1/?expand=department')
        assert response.status_code == 200
        assert len(response.data) == 1
        assert response.data == expected_office_1_response


class TestViewDepartments:
    def test_view_departments(self, api_client):
        response = api_client.get('/departments/')
        assert response.status_code == 200
        assert len(response.data) == 11
        assert response.data == settings.DEPARTMENTS

    def test_view_departments_with_pagination(self, api_client):
        response = api_client.get('/departments/?limit=8&offset=1')
        assert response.status_code == 200
        assert len(response.data) == 8
        assert response.data == settings.DEPARTMENTS[1:9]

    def test_view_departments_with_wrong_expansions(self, api_client):
        with pytest.raises(KeyError) as error:
            api_client.get('/departments/?expand=department')
        assert "Expansion department not allowed for department entity" in str(error.value)

    def test_view_department_ASdasdas(self, api_client, expected_department_1_response):
        response = api_client.get('/departments/7/')
        assert response.status_code == 200
        assert len(response.data) == 1
        assert response.data == expected_department_1_response

    def test_view_department_with_pagination(self, api_client, expected_department_1_response):
        # The request with an id, should ignore pagination querypaarams
        response = api_client.get('/departments/7/?limit=2&offset=1')
        assert response.status_code == 200
        assert len(response.data) == 1
        assert response.data == expected_department_1_response

    def test_view_department_with_multi_expansions(
        self,
        api_client,
        expected_response_with_extreme_expansions
    ):
        # It should return the same data, and ignore the expand queryparam
        response = api_client.get('/departments/?expand=superdepartment.superdepartment.superdepartment')
        assert response.status_code == 200
        assert len(response.data) == 11
        assert response.data == expected_response_with_extreme_expansions

    def test_view_department_with_expansions(
        self,
        api_client,
        expected_department_1_response_with_expansion
    ):
        # It should return the same data, and ignore the expand queryparam
        response = api_client.get('/departments/7/?expand=superdepartment')
        assert response.status_code == 200
        assert len(response.data) == 1
        assert response.data == expected_department_1_response_with_expansion


class TestViewsEmployees:

    def test_view_employees(self, api_client, requests_mock, stubbed_response_from_employee_api):
        stub_employee_response = stubbed_response_from_employee_api()
        mock_request = requests_mock.get(settings.EMPLOYEE_SOURCE, json=stub_employee_response)
        response = api_client.get('/employees/')
        assert response.status_code == 200
        assert len(response.data) == 100
        assert mock_request.call_count == 1

    def test_view_employees_with_pagination(self, api_client, requests_mock, stubbed_response_from_employee_api):
        limit = 2
        offset = 100
        stub_employee_response = stubbed_response_from_employee_api(limit=limit, offset=offset)
        mock_request = requests_mock.get(settings.EMPLOYEE_SOURCE, json=stub_employee_response)
        response = api_client.get('/employees/?limit={}&offset={}'.format(limit, offset))
        assert response.status_code == 200
        assert len(response.data) == 2
        assert mock_request.call_count == 1
        assert response.data[0]['id'] == 101

    def test_view_employees_with_expansions(self, api_client, requests_mock, stubbed_response_from_employee_api):
        limit = 1
        stub_employee_response = stubbed_response_from_employee_api(limit=limit)
        mock_request = requests_mock.get(settings.EMPLOYEE_SOURCE, json=stub_employee_response)
        response = api_client.get('/employees/?limit={}&expand=office'.format(limit))
        assert response.status_code == 200
        assert len(response.data) == 1
        assert mock_request.call_count == 1
        # check that office was expanded
        assert type(response.data[0]['office']) == dict

    def test_view_employee(self, api_client, requests_mock, stubbed_response_from_employee_api):
        stub_employee_response = stubbed_response_from_employee_api(pk=1)
        mock_request = requests_mock.get(settings.EMPLOYEE_SOURCE, json=stub_employee_response)
        response = api_client.get('/employees/1/')
        assert response.status_code == 200
        assert len(response.data) == 1
        assert mock_request.call_count == 1

    def test_view_employee_with_pagination(self, api_client, requests_mock, stubbed_response_from_employee_api):
        # When you hit the api with limit and offset, but with an specific id it will return employee with the given id
        limit = 2
        offset = 100
        stub_employee_response = stubbed_response_from_employee_api(limit=limit, offset=offset, pk=1)
        mock_request = requests_mock.get(settings.EMPLOYEE_SOURCE, json=stub_employee_response)
        response = api_client.get('/employees/1/?limit={}&offset={}'.format(limit, offset))
        assert response.status_code == 200
        assert len(response.data) == 1
        assert mock_request.call_count == 1
        assert response.data[0]['id'] == 1

    def test_view_employee_with_expansions(self, api_client, requests_mock, stubbed_response_from_employee_api):
        stub_employee_response = stubbed_response_from_employee_api(pk=1)
        mock_request = requests_mock.get(settings.EMPLOYEE_SOURCE, json=stub_employee_response)
        response = api_client.get('/employees/1/?expand=department')
        assert response.status_code == 200
        assert len(response.data) == 1
        assert mock_request.call_count == 1
        # check that department was expanded
        assert type(response.data[0]['department']) == dict
