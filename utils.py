from rest_framework.throttling import AnonRateThrottle


# Throttle the anon request to our api, this could be changed in our settings
class EmployeesThrottle(AnonRateThrottle):
    scope = 'employees'
